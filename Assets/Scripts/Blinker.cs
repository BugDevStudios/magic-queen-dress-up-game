﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Blinker : MonoBehaviour
{
    float timer;
    bool flag=false;
    int Count = 0;
    Color color1,color2;
	void Start ()
    {
        color1 = this.gameObject.GetComponent<Image>().color;
        color2 = Color.yellow;
        timer = Time.time;
    }
	
	void Update ()
    {
        if (Time.time - timer > 1f)
        {
            flag = !flag;
            timer = Time.time;
            Count++;
            if (flag == true)
            {
                if (gameObject.activeSelf == true)
                {
                    gameObject.GetComponent<Image>().color = color1;
                }
            }
            else if (flag == false)
            {
                if (gameObject.activeSelf == true)
                {
                    gameObject.GetComponent<Image>().color = color2;
                }
            }
            Debug.Log(Count);
        }
	}
}
