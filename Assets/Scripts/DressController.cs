﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine.UI;

public class DressController : MonoBehaviour {
    public static DressController instance;
    public GameObject DressMenu,ShoesMenu;
    void Awake()
    {
        instance = this;
    }
	void Start ()
    {
	
	}
	void Update ()
    {
	
	}
    public void OnClickDress()
    {
        GameObject Clicked = EventSystem.current.currentSelectedGameObject;
        if (UI_Control.instance.MenuOpen == true)
        {
            UI_Control.instance.OpenMenu();
        }
        bool Available = false;
        if (Clicked.GetComponent<LockController>().Locked == true)
        {
            Debug.Log("ITEM IS LOCKED");
            //UI_Control.instance.OpenPurchaseDialog(Clicked);
            UI_Control.instance.OpenAskYourParents(Clicked);
            UI_Control.instance.CurrentlyShownProduct = Clicked;
        }
        else if (Clicked.GetComponent<LockController>().Locked == false)
        {
            Available = true;
        }
		if (Clicked.tag == "Dress") {
			if (Available == true) {
				if (UI_Control.instance.BodySkinTone0.activeSelf == true) {
					UI_Control.instance.BodySkinTone0.GetComponent<SpriteRenderer> ().sprite = UI_Control.instance.Naked0;
				}
				if (UI_Control.instance.BodySkinTone1.activeSelf == true) {
					UI_Control.instance.BodySkinTone1.GetComponent<SpriteRenderer> ().sprite = UI_Control.instance.Naked1;
				}
				if (UI_Control.instance.BodySkinTone2.activeSelf == true) {
					UI_Control.instance.BodySkinTone2.GetComponent<SpriteRenderer> ().sprite = UI_Control.instance.Naked2;
				}
				if (UI_Control.instance.BodySkinTone3.activeSelf == true) {
					UI_Control.instance.BodySkinTone3.GetComponent<SpriteRenderer> ().sprite = UI_Control.instance.Naked3;
				}
				if (Clicked.name == "Dress1") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [0].SetActive (true);
					UI_Control.instance.Dresses [0].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (1)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [1].SetActive (true);
					UI_Control.instance.Dresses [1].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (2)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [2].SetActive (true);
					UI_Control.instance.Dresses [2].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (3)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [3].SetActive (true);
					UI_Control.instance.Dresses [3].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (4)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [4].SetActive (true);
					UI_Control.instance.Dresses [4].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (5)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [5].SetActive (true);
					UI_Control.instance.Dresses [5].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (6)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [6].SetActive (true);
					UI_Control.instance.Dresses [6].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (7)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [7].SetActive (true);
					UI_Control.instance.Dresses [7].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (8)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [8].SetActive (true);
					UI_Control.instance.Dresses [8].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (9)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [9].SetActive (true);
					UI_Control.instance.Dresses [9].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (10)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [10].SetActive (true);
					UI_Control.instance.Dresses [10].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (11)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [11].SetActive (true);
					UI_Control.instance.Dresses [11].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Dress1 (12)") {
					foreach (GameObject item in UI_Control.instance.Dresses) {
						item.SetActive (false);
					}
					UI_Control.instance.Dresses [12].SetActive (true);
					UI_Control.instance.Dresses [12].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				}
			}
		} else if (Clicked.tag == "Shoes") {
			if (Available == true) {
				if (Clicked.name == "Shoes1") {
					foreach (GameObject item in UI_Control.instance.Shoes) {
						item.SetActive (false);
					}
					UI_Control.instance.Shoes [0].SetActive (true);
					UI_Control.instance.Shoes [0].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Shoes1 (1)") {
					foreach (GameObject item in UI_Control.instance.Shoes) {
						item.SetActive (false);
					}
					UI_Control.instance.Shoes [1].SetActive (true);
					UI_Control.instance.Shoes [1].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Shoes1 (2)") {
					foreach (GameObject item in UI_Control.instance.Shoes) {
						item.SetActive (false);
					}
					UI_Control.instance.Shoes [2].SetActive (true);
					UI_Control.instance.Shoes [2].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Shoes1 (3)") {
					foreach (GameObject item in UI_Control.instance.Shoes) {
						item.SetActive (false);
					}
					UI_Control.instance.Shoes [3].SetActive (true);
					UI_Control.instance.Shoes [3].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Shoes1 (4)") {
					foreach (GameObject item in UI_Control.instance.Shoes) {
						item.SetActive (false);
					}
					UI_Control.instance.Shoes [4].SetActive (true);
					UI_Control.instance.Shoes [4].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				} else if (Clicked.name == "Shoes1 (5)") {
					foreach (GameObject item in UI_Control.instance.Shoes) {
						item.SetActive (false);
					}
					UI_Control.instance.Shoes [5].SetActive (true);
					UI_Control.instance.Shoes [5].GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
				}
			}
		}
    }
    public void OnSelectDressUp()
    {
        GameObject Clicked = EventSystem.current.currentSelectedGameObject;
        if (Clicked.name == "Shoes")
        {
            DressMenu.SetActive(false);
            ShoesMenu.SetActive(true);
        }
        else if (Clicked.name == "Dresses")
        {
            ShoesMenu.SetActive(false);
            DressMenu.SetActive(true);
        }
    }
}
