﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DropController : MonoBehaviour {
    public static DropController instance; 
    public Vector3 OrigPos;
    public bool flag;
    void Awake()
    {
        instance = this;
    }
	void Start ()
    {
        OrigPos = transform.position;
        gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        flag = true;
	}
	
	void Update ()
    {
        if (flag == true)
        {
            this.gameObject.transform.DOMoveY(OrigPos.y - 0.5f, 1.5f).OnComplete(() => RestorePos());
        }
        flag = false;
    }

    void RestorePos()
    {
        transform.position = OrigPos;
        flag = true;
    }

}
