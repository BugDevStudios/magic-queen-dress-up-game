﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HairController : MonoBehaviour
{
    public static HairController instance;
    public GameObject ShampooDrop, Foam, Face, Neck, ShoweringScene;
    public Camera SpritesCam;
    public float TimeLeft;
    public bool flag, ScrubbingDone=false;
    public int count=0;
    void Awake()
    {
        instance = this;
    }
	void Start ()
    {
        flag = false;
	}	
	void Update ()
    {
        TimeLeft -= Time.deltaTime;
        if (TimeLeft < 0)
        {
            flag = true;
            TimeLeft = 0.5f;
        }
        else
        {
            flag = false;
        }
    }
    void OnMouseDrag()
    {
        if (UI_Control.instance.Scrub == true)
        {
            if ((gameObject.GetComponent<Collider2D>().bounds.Contains(new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10)) == true) && (Face.GetComponent<Collider2D>().bounds.Contains(new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10)) == false) && (Neck.GetComponent<Collider2D>().bounds.Contains(new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10)) == false))
            {
                if (Foam.activeSelf == false)
                {
                    Foam.SetActive(true);
                }
                Foam = Instantiate(Foam, new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10), Quaternion.Euler(0, 0, 0)) as GameObject;
                if (Foam.GetComponent<SpriteRenderer>().color.a!=1)
                {
                    Foam.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                }
                FoamController.instance.FoamParticles.Add(Foam);
                Foam.transform.SetParent(ShoweringScene.transform);
                ScrubbingDone = true;
                if (ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color.a != 0)
                {
                    ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                }
            }
        }
    }
    void OnMouseUp()
    {
        ShowerScene.instance.Hand.transform.position = new Vector3(ShowerScene.instance.ShowerObj.transform.position.x, ShowerScene.instance.ShowerObj.transform.position.y, ShowerScene.instance.ShowerObj.transform.position.z);
        ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        ShampooController.instance.ScrubbingAvailable = false;
    }
    void OnMouseDown()
    {
        if ((ShampooController.instance.ScrubbingAvailable == true))
        {
            if ((gameObject.GetComponent<Collider2D>().bounds.Contains(new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10)) == true) && (Face.GetComponent<Collider2D>().bounds.Contains(new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10)) == false) && (Neck.GetComponent<Collider2D>().bounds.Contains(new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10)) == false))
            {
                if (Foam.activeSelf == false)
                {
                    Foam.SetActive(true);
                }
                Foam = Instantiate(Foam, new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10), Quaternion.Euler(0, 0, 0)) as GameObject;
                Foam.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                Foam.transform.SetParent(ShoweringScene.transform);
                FoamController.instance.FoamParticles.Add(Foam);
            }
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if ((other.name == "Image_Shampoo")&&(flag==true))
        {
            Instantiate(ShampooDrop,new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x-0.45f, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y+0.2f, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10), Quaternion.Euler(0,0,0));
            ShampooController.instance.ScrubbingAvailable = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag=="ShampooDrop")
        {
            other.gameObject.SetActive(false);
            ShowerScene.instance.ShampooApplied = true;
        }
    }
}
