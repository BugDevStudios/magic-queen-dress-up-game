﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LockController : MonoBehaviour {
    
    public bool Locked = false;
    public GameObject LockedIcon;
    int count = 0;

    void Awake()
    {
        LockedIcon = new GameObject();
    }

    void Update()
    {
        if (Locked == true&&count==0)
        {
            LockedIcon = (GameObject)Resources.Load("LockIcon");
            LockedIcon = Instantiate(LockedIcon, gameObject.transform);
            LockedIcon.transform.localScale = new Vector3(1,1,1);
            LockedIcon.transform.position = gameObject.transform.position;
            LockedIcon.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
            LockedIcon.GetComponent<RectTransform>().anchorMax = new Vector2(1, 0);
            LockedIcon.GetComponent<RectTransform>().anchorMin = new Vector2(1, 0);
            count++;
        }
    }

    public void LockGameObject()
    {
        Locked = true;
    }
    public void UnLockGameObject()
    {
        Locked = false;
    }
}
