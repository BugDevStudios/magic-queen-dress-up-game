﻿using UnityEngine;
using System.Collections;

public class RefreshGallery
{

    #if UNITY_ANDROID
    static AndroidJavaClass _plugin;
    
    static RefreshGallery()
    {
        _plugin = new AndroidJavaClass("com.refreshgallary.MainActivity");
    }
    
    public static void refreshGallery(string path , string name)
    {
        _plugin = new AndroidJavaClass("com.refreshgallary.MainActivity");

        var activityJavaClass=new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        if(activityJavaClass!=null)
        {
            Debug.Log("UnityPlayer class not null");
        }
        Debug.Log("Arslan::refreshGallery1");
        var currentActivityJavaObject=activityJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
        AndroidJavaObject app = currentActivityJavaObject.Call<AndroidJavaObject>("getApplicationContext");

        Debug.Log("Arslan::refreshGallery2");
        currentActivityJavaObject.Call("runOnUiThread",new AndroidJavaRunnable(() =>{
            Debug.Log("Arslan::refreshGallery3");
            _plugin.CallStatic("refreshGallery",app,path,name);
        }));


    }
    #endif
}
