﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShowerController : MonoBehaviour
{
    public static ShowerController instance;
    public Camera SpritesCam;
    public Vector3 OrigPosition, OrigParticlePosition;
    public GameObject Shadow;
    public ParticleSystem[] Water = new ParticleSystem[4];
    public GameObject[] Drop;
    void Awake()
    {
        Shadow = GameObject.Find("Image_ShowerShadow");
        instance = this;
    }
    void Start()
    {
        OrigPosition = transform.position;
        OrigParticlePosition = Water[0].transform.position;
    }
    void Update()
    {

    }
    void OnMouseDown()
    {

    }
    void OnMouseDrag()
    {
        transform.position = new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10);
        Shadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        if (HairController.instance.ScrubbingDone == true)
        {
            ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            foreach (ParticleSystem item in Water)
            {
                item.enableEmission = true;
            }
            StartCoroutine(FadeIn(ShowerScene.instance.WetHair));
            foreach (GameObject item in Drop)
            {
                if (item.activeSelf == false)
                {
                    item.SetActive(true);
                }
                StartCoroutine(FadeIn(item));
            }
            GameObject[] Foam = GameObject.FindGameObjectsWithTag("Foam");
            foreach (GameObject item in Foam)
            {
                StartCoroutine(FadeOut(item));
            }
        }
    }
    void OnMouseUp()
    {
        StopAllCoroutines();
        iTween.MoveTo(transform.gameObject, OrigPosition, 0.5f);
        Shadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        foreach (ParticleSystem item in Water)
        {
            item.enableEmission = false;
        }
        if (HairController.instance.ScrubbingDone == true)
        { 
            TowelController.instance.HairIsWet = true;
            ShowerScene.instance.WetHair.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            GameObject[] Foam = GameObject.FindGameObjectsWithTag("Foam");
            foreach (GameObject item in Drop)
            {
                if (item.activeSelf == false)
                {
                    item.SetActive(true);
                }
                item.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            }
            foreach (GameObject item in Foam)
            {
                if (item.GetComponent<SpriteRenderer>().color.a < 0.1f)
                {
                    item.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                }
            }
            ShowerScene.instance.Hand.transform.position = ShowerScene.instance.Towel.transform.position;
            ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }
        ShowerScene.instance.Dragging = false;
    }
    public IEnumerator FadeOut(GameObject item)
    {
        float StartTime = Time.time;
        while (Time.time - StartTime < 3f)
        {
            item.GetComponent<SpriteRenderer>().color -= new Color(0, 0, 0, Time.deltaTime*0.005f);
            yield return null;
        }
    }
    public IEnumerator FadeIn(GameObject item)
    {
        float StartTime = Time.time;
        while (Time.time - StartTime < 3f)
        {
            item.GetComponent<SpriteRenderer>().color += new Color(0, 0, 0, Time.deltaTime * 0.005f);
            yield return null;
        }
    }
}
