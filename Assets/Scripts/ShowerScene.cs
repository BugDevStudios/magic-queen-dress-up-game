﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using DG.Tweening;

public class ShowerScene : MonoBehaviour
{
    #region Declarations
    public static ShowerScene instance;
    public GameObject SelectedModel, Clicked, Shampoo, ShampooShadow, ShowerObj, ShowerShadow, Towel, TowelShadow, Hand, Hair, WetHair;
    public bool Dragging = false, ShampooApplied=false;
    public GameObject ShowerSceneObj;
    #endregion Declarations
    void Awake()
    {
        instance = this;
        ShowerSceneObj = GameObject.Find("ShowerScene");
    }
    void Start()
    {
        WetHair.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
    }
    void Update()
    {
        Clicked = EventSystem.current.currentSelectedGameObject;
    }

    public void HoverHand(GameObject gameObject)
    {
        if (UI_Control.instance.HandInstantiated==false)
        {
            Hand = Instantiate(Hand, new Vector3(gameObject.transform.position.x - 0.25f, gameObject.transform.position.y - 0.25f, gameObject.transform.position.z), gameObject.transform.rotation) as GameObject;
            Hand.transform.SetParent(ShowerSceneObj.transform);
            Hand.gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "ShampooScreen";
            Hand.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 6;
            Hand.transform.position = Shampoo.transform.position;
            StartCoroutine(SHM());
            UI_Control.instance.HandInstantiated = true;
        }
    }
    public IEnumerator SHM()
    {
        while (Dragging == false)
        {
            yield return new WaitForSeconds(0f);
            float StartTime = Time.time;
            while (Time.time - StartTime < 0.5f)
            {
                Hand.transform.DOBlendableLocalMoveBy(new Vector3(0.025f, 0.025f, 0), 0.25f);
                yield return null;
            }

            StartTime = Time.time;

            while (Time.time - StartTime < 0.5f)
            {
                Hand.transform.DOBlendableLocalMoveBy(new Vector3(-0.025f, -0.025f, 0), 0.25f);
                yield return null;
            }
        }
    }
}
