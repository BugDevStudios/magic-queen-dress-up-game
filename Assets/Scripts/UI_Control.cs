﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System.IO;
using DeadMosquito.AndroidGoodies;

public class UI_Control : MonoBehaviour {
    #region Declarations
    public GameObject Button_Play, Button_Store, Button_Settings, Button_Quit, NameField, Button_Enter, Panel_MainMenu, ModelSelection_BG, Panel_ModelSelection, Panel_Shower, SkinTone0, SkinTone1, SkinTone2, SkinTone3, Panel_MakeUp_Main, Panel_MakeUp_Sidebar, SelectDressScene;
    public GameObject SelectedModel, Table;
    public GameObject ShoweringScene, DressUpScene;
    public GameObject HairStyle, LipStick, LipGloss, EyeLens, HairBand, Glasses, EyeBrows, Earrings, EyeShadow, EyeLiner, EyeLashes, Blushes;
    public GameObject BodySkinTone0, BodySkinTone1, BodySkinTone2, BodySkinTone3, MKTone0, MKTone1, MKTone2, MKTone3;
    public Text NameFieldText;
    public string UserName;
    public bool Faded=false, HandInstantiated=false;
    public static UI_Control instance;
    public enum Stage {StartStage, ModelSelectStage,ShowerStage, MakeUpStage, DressUpStage, ScreenShotStage};
    public Stage CurrentStage;
    public List<GameObject> AppliedMakeUp;
    public bool MenuOpen = false; public Sprite MenuIcon, CrossIcon; public GameObject MenuButton, VolumeButton, CartButton, HomeButton, ScreenShotButton, RateButton;
    Vector3 CamPos, VolPos, HomePos, CartPos, RatePos;
    public GameObject ScreenShotFrame, ShareButton, SaveToGalleryButton;
    public Canvas MainCanvas;
    public Text Deb;
    string Screen_Shot_File_Name;
    public Camera MainCamera;
    bool MenuNavigationAvailable=true;
    public bool ScreenShotTaken;
    public GameObject PrevButton, NextButton;
    public GameObject Panel_Dresses, DressCloset, Panel_Shoes;
    public GameObject[] Dresses, Shoes;
    public Sprite Naked0, Naked1, Naked2, Naked3;
    public Sprite Reg0, Reg1, Reg2, Reg3;
    public bool Scrub=false;
    public GameObject Mask, Panel_DressUpMain; bool BackAvailable=true;float BackTimer;
    public GameObject DressUpSelectionImage;
    int Activated = 1;
    Vector3 OrigPosStart, OrigPosSettings, OrigPosQuit, OrigPosStore, OrigPosCoins, OrigPosNameField,OrigPosEnter;
    public int Coins,count;
    


    public GameObject Panel_PurchaseThisItem;
    public GameObject ItemImage,PriceText,BuyButton;
    public GameObject CurrentlyShownProduct;
    public GameObject NameText;
    public AudioSource ShootingStar, BGMusic;
    public Sprite VolumeOff,VolumeOn;

    public GameObject[] Purchasables;
    public GameObject[] ShowerSceneSprites;
    public GameObject[] MakeUpSceneSprites;
    public GameObject[] DressUpSceneSprites;

    public GameObject[] DressUpScenePanel;
    public GameObject[] ShoesPanel;
    public GameObject[] HairPanel;
    public GameObject[] LipStickPanel;

    public GameObject[] LipGlossPanel;
    public GameObject[] EyeLensPanel;
    public GameObject[] HairBandPanel;
    public GameObject[] GlassesPanel;
    public GameObject[] EyeBrowsPanel;
    public GameObject[] EarringsPanel;
    public GameObject[] EyeShadowPanel;
    public GameObject[] EyeLinerPanel;
    public GameObject[] EyeLashesPanel;
    public GameObject[] BlushesPanel;


    Texture2D Tex;

    Dictionary<string, object> options = new Dictionary<string, object>();
    Dictionary<string, object> options2 = new Dictionary<string, object>();

    string DressLocker, ShoesLocker, HairLocker,LipStickLocker,LipGlossLocker,EyeLensLocker,HairBandLocker, GlassesLocker,EyeBrowsLocker, EarringsLocker, EyeShadowLocker, EyeLinerLocker, EyeLashesLocker, BlushesLocker;
    char[] T;
    bool NoUnlocks=true;
    GameObject ToBeUnlocked;
    public GameObject Panel_Unlocked, Panel_UnlockAll, UnlockAllButton;
    public Image UnlockedItem;

    public List<Text> askYourParentsText;
    public InputField answerField;
    public GameObject askYourParentsPopUp;
    int correctAnswer;bool allProducts;
    GameObject clickedProduct;

    #endregion Declarations
    void Awake()
    {
        instance = this;
        Button_Play = GameObject.Find("btn_HomeScreen_Play");
        Button_Store = GameObject.Find("btn_HomeScreen_Store");
        Button_Settings = GameObject.Find("btn_HomeScreen_Settings");
        Button_Quit = GameObject.Find("btn_HomeScreen_Quit");
        //Button_GetFreeCoins = GameObject.Find("btn_HomeScreen_GetFreeCoins");
        Button_Enter = GameObject.Find("btn_HomeScreen_Enter");
        Panel_MainMenu = GameObject.Find("Panel_MainMenu");
        NameField = GameObject.Find("NameField");
        Panel_ModelSelection = GameObject.Find("Panel_ModelSelection");
        Panel_Shower = GameObject.Find("Panel_Shower");
        DressUpScene = GameObject.Find("MakeUpScene");
        SelectDressScene = GameObject.Find("SelectDressScene");
        PlayerPrefs.GetInt("count", count);

        DressLocker = PlayerPrefs.GetString("DressLocker", "00000000111");
        ShoesLocker = PlayerPrefs.GetString("ShoesLocker", "000001");
        HairLocker = PlayerPrefs.GetString("HairLocker", "00000000111111");
        LipStickLocker = PlayerPrefs.GetString("LipStickLocker", "0000000001111");
        LipGlossLocker = PlayerPrefs.GetString("LipGlossLocker", "0000000000000");
        EyeLensLocker = PlayerPrefs.GetString("EyeLensLocker", "0000000000000");
        HairBandLocker = PlayerPrefs.GetString("HairBandLocker","0000011");
        GlassesLocker = PlayerPrefs.GetString("GlassesLocker", "0000");
        EyeBrowsLocker = PlayerPrefs.GetString("EyeBrowsLocker", "00000000");
        EarringsLocker = PlayerPrefs.GetString("EarringsLocker", "000000000");
        EyeShadowLocker = PlayerPrefs.GetString("EyeShadowLocker","0000000001111");
        EyeLinerLocker = PlayerPrefs.GetString("EyeLinerLocked","000000000");
        EyeLashesLocker = PlayerPrefs.GetString("EyeLashesLocker","0000000");
        BlushesLocker = PlayerPrefs.GetString("BlushesLocker", "0000000001111");

        
        //DEMO CODE: UNLOCKS EVERYTHING
        //foreach (GameObject item in Purchasables)
        //{
        //    foreach (Transform item2 in item.transform.GetChild(0))
        //    {
        //        item2.gameObject.GetComponent<LockController>().Locked = false;
        //    }
        //}
    }
    void Start()
    {
        NameField.SetActive(false);
        Button_Enter.SetActive(false);
        DressUpScene.SetActive(false);
        SelectDressScene.SetActive(false);
        HidePanel(Panel_ModelSelection);
        if (VolumeButton)
        {
            VolPos = VolumeButton.transform.position;
        }
        CamPos = ScreenShotButton.transform.position;
        RatePos = RateButton.transform.position;
        CartPos = CartButton.transform.position;
        HomePos = HomeButton.transform.position;
        VolumeButton.SetActive(false);
        CartButton.SetActive(false);
        HomeButton.SetActive(false);
        ScreenShotButton.SetActive(false);
        RateButton.SetActive(false);
        ScreenShotFrame.SetActive(false);
        CurrentStage = Stage.StartStage;
        OrigPosStart = Button_Play.transform.position;
        OrigPosStore = Button_Store.transform.position;
        OrigPosSettings = Button_Settings.transform.position;
        OrigPosQuit = Button_Quit.transform.position;
        //OrigPosCoins = Button_GetFreeCoins.transform.position;
        OrigPosEnter = Button_Enter.transform.position;
        OrigPosNameField = NameField.transform.position;
        Vungle.init("58a529cbbebd8ce537000098", "", "");
        SetVungleAdOptions();
        CheckLockedItems();
    }

    void CheckLockedItems()
    {
        for (int i = 0; i < DressLocker.Length; i++)
        {
            if (DressLocker[i].ToString()=="0")
            {
                DressUpScenePanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (DressLocker[i].ToString() == "1")
            {
                DressUpScenePanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < ShoesLocker.Length; i++)
        {
            if (ShoesLocker[i].ToString() == "0")
            {
                ShoesPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (ShoesLocker[i].ToString() == "1")
            {
                ShoesPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < HairLocker.Length; i++)
        {
            if (HairLocker[i].ToString() == "0")
            {
                HairPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (HairLocker[i].ToString() == "1")
            {
                HairPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < LipStickLocker.Length; i++)
        {
            if (LipStickLocker[i].ToString() == "0")
            {
                LipStickPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (LipStickLocker[i].ToString() == "1")
            {
                LipStickPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < LipGlossLocker.Length; i++)
        {
            if (LipGlossLocker[i].ToString() == "0")
            {
                LipGlossPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (LipGlossLocker[i].ToString() == "1")
            {
                LipGlossPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < EyeLensLocker.Length; i++)
        {
            if (EyeLensLocker[i].ToString() == "0")
            {
                EyeLensPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (EyeLensLocker[i].ToString() == "1")
            {
                EyeLensPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < HairBandLocker.Length; i++)
        {
            if (HairBandLocker[i].ToString() == "0")
            {
                HairBandPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (HairBandLocker[i].ToString() == "1")
            {
                HairBandPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < GlassesLocker.Length; i++)
        {
            if (GlassesLocker[i].ToString() == "0")
            {
                GlassesPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (GlassesLocker[i].ToString() == "1")
            {
                GlassesPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < EyeBrowsLocker.Length; i++)
        {
            if (EyeBrowsLocker[i].ToString() == "0")
            {
                EyeBrowsPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (EyeBrowsLocker[i].ToString() == "1")
            {
                EyeBrowsPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < EarringsLocker.Length; i++)
        {
            if (EarringsLocker[i].ToString() == "0")
            {
                EarringsPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (EarringsLocker[i].ToString() == "1")
            {
                EarringsPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < EyeShadowLocker.Length; i++)
        {
            if (EyeShadowLocker[i].ToString() == "0")
            {
                EyeShadowPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (EyeShadowLocker[i].ToString() == "1")
            {
                EyeShadowPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < EyeLinerLocker.Length; i++)
        {
            if (EyeLinerLocker[i].ToString() == "0")
            {
                EyeLinerPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (EyeLinerLocker[i].ToString() == "1")
            {
                EyeLinerPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < EyeLashesLocker.Length; i++)
        {
            if (EyeLashesLocker[i].ToString() == "0")
            {
                EyeLashesPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (EyeLashesLocker[i].ToString() == "1")
            {
                EyeLashesPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
        for (int i = 0; i < BlushesLocker.Length; i++)
        {
            if (BlushesLocker[i].ToString() == "0")
            {
                BlushesPanel[i].GetComponent<LockController>().Locked = false;
            }
            else if (BlushesLocker[i].ToString() == "1")
            {
                BlushesPanel[i].GetComponent<LockController>().Locked = true;
            }
        }
    }
    void Update()
    {
        if (CurrentStage != Stage.StartStage)
        {
            if (ShootingStar.isPlaying == true)
            {
                MenuNavigationAvailable = false;
            }
            else if (ShootingStar.isPlaying == false)
            {
                MenuNavigationAvailable = true;
            }
        }
        if (MenuNavigationAvailable == true)
        {
            if (Input.GetKey(KeyCode.Escape) && (BackAvailable==true))
            {
                Debug.Log(CurrentStage.ToString());
                if (CurrentStage == Stage.StartStage)
                {
                    Application.Quit();
                }
                else if (CurrentStage == Stage.ModelSelectStage)
                {
                    GoToHomePage();
                }
                else
                {
                    LoadPrevScene();
                    if (CurrentStage == Stage.ShowerStage)
                    {
                        CurrentStage = Stage.ModelSelectStage;
                    }
                    else if (CurrentStage == Stage.ModelSelectStage)
                    {
                        CurrentStage = Stage.StartStage;
                    }
                }
                BackTimer = Time.time;
                BackAvailable = false;
            }
        }
        if (Time.time - BackTimer > 0.5f)
        {
            BackAvailable = true;
        }
        
        if (Input.GetKey(KeyCode.Z))
        {
            Debug.Log(CurrentStage.ToString());
        }
    }
    void SetVungleAdOptions()
    {
        options["incentivized"] = true;
        options2["intentivized"] = false;
		//Debug- waleed
//        options2["orientation"] = VungleAdOrientation.MatchVideo;
//        options["orientation"] = VungleAdOrientation.MatchVideo;
        Vungle.onAdFinishedEvent += Vungle_onAdFinishedEvent;
        
    }
    private void Vungle_onAdFinishedEvent(AdFinishedEventArgs obj)
    {
        if (obj.IsCompletedView == true)
        {
            if (NoUnlocks == true)
            {

            }
            else if (NoUnlocks == false)
            {
                ToBeUnlocked.GetComponent<LockController>().Locked = false;
                ToBeUnlocked.GetComponent<LockController>().LockedIcon.SetActive(false);
                Panel_PurchaseThisItem.SetActive(false);
                Panel_Unlocked.SetActive(true);
                UnlockedItem.GetComponent<Image>().sprite = ToBeUnlocked.GetComponent<Image>().sprite;
                UnlockedItem.GetComponent<Image>().preserveAspect = true;
            }
        }
        
        //throw new System.NotImplementedException();
    }
    public void PlayVungleAd()
    {
        if (NoUnlocks == false)
        {
            Vungle.playAdWithOptions(options);
        }
        else if (NoUnlocks == true)
        {
            Vungle.playAdWithOptions(options2);
        }
    }
    public void PlayGame()
    {
        
        StartCoroutine(TakeMenuAway());
        NameField.SetActive(true);
        NameField.transform.position = new Vector3(NameField.transform.position.x, NameField.transform.position.y + 20, NameField.transform.position.z);
        Button_Enter.SetActive(true);
        Button_Enter.transform.position = new Vector3(Button_Enter.transform.position.x, Button_Enter.transform.position.y - 10, Button_Enter.transform.position.z);
        iTween.MoveTo(NameField, new Vector3(0, 0, 0), 0.5f);
        iTween.MoveTo(Button_Enter, new Vector3(0, -1, 0), 0.75f);
        //NameFieldText.GetComponent<Text>().color = new Color(0, 0, 0, 1);
    }
    public void EnterGame()
    {
        UserName = NameFieldText.text;
        bool fieldMoving = false;
        CurrentStage = Stage.ModelSelectStage;
        if (NameFieldText.text != "")
        {
            fieldMoving = true;
            iTween.MoveBy(NameField, new Vector3(0, 1090, 0), 0.75f);
            iTween.MoveBy(Button_Enter, new Vector3(0, -1090, 0), 0.75f);
            StartCoroutine(FadeOut(Panel_MainMenu));
            StartCoroutine(FadeIn(Panel_ModelSelection, Panel_MainMenu, ""));
        }
        if (fieldMoving == false)
        {
            EventSystem.current.SetSelectedGameObject(NameField.gameObject, null);
        }
    }
    public void ModelSelect()
    {
        if (ShootingStar.isPlaying == false)
        {
            ShootingStar.Play();
        }
        if (MenuOpen == true)
        {
            OpenMenu();
        }
        foreach (GameObject item in ShowerSceneSprites)
        {
            if (item.activeSelf == false)
            {
                item.SetActive(true);
                item.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            }
        }
        Scrub = true;
        SkinTone0.SetActive(false);
        SkinTone1.SetActive(false);
        SkinTone2.SetActive(false);
        SkinTone3.SetActive(false);
        
        string Name = EventSystem.current.currentSelectedGameObject.GetComponent<Transform>().name;
        StartCoroutine(FadeOut(Panel_ModelSelection));
        StartCoroutine(FadeIn(Panel_Shower, Panel_ModelSelection, Name));
        ShowerScene.instance.HoverHand(ShowerScene.instance.Shampoo);
        ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
        ShowerScene.instance.Hand.transform.position = new Vector3(ShowerScene.instance.Shampoo.transform.position.x - 0.25f, ShowerScene.instance.Shampoo.transform.position.y - 0.25f, ShowerScene.instance.Shampoo.transform.position.z);
        CurrentStage = Stage.ShowerStage;
        
    } 
    public void HidePanel(GameObject gameObject)
    {
        gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        foreach (Transform item in gameObject.transform)
        {
            item.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        }
    }
    public void LoadPrevScene()
    {
        if (ScreenShotFrame.activeSelf == true)
        {
            ScreenShotFrame.SetActive(false);
        }
        if (MainCanvas.enabled == false)
        {
            MainCanvas.enabled = true;
        }
        if (CurrentStage != Stage.ScreenShotStage)
        {
            if (UI_Control.instance.BodySkinTone0.activeSelf == true)
            {
                UI_Control.instance.BodySkinTone0.GetComponent<SpriteRenderer>().sprite = UI_Control.instance.Reg0;
            }
            if (UI_Control.instance.BodySkinTone1.activeSelf == true)
            {
                UI_Control.instance.BodySkinTone1.GetComponent<SpriteRenderer>().sprite = UI_Control.instance.Reg1;
            }
            if (UI_Control.instance.BodySkinTone2.activeSelf == true)
            {
                UI_Control.instance.BodySkinTone2.GetComponent<SpriteRenderer>().sprite = UI_Control.instance.Reg2;
            }
            if (UI_Control.instance.BodySkinTone3.activeSelf == true)
            {
                UI_Control.instance.BodySkinTone3.GetComponent<SpriteRenderer>().sprite = UI_Control.instance.Reg3;
            }
        }
        

        if (MenuNavigationAvailable == true)
        {
            if (MenuOpen == true)
            {
                OpenMenu();
            }
            ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            if (CurrentStage == Stage.ModelSelectStage || CurrentStage == Stage.ShowerStage)
            {
                if (ShootingStar.isPlaying == false)
                {
                    ShootingStar.Play();
                }
                Scrub = false;
                if (MenuOpen == true)
                {
                    OpenMenu();
                }
                foreach (Transform item in ShoweringScene.transform)
                {
                    if (item.gameObject.tag == "Foam")
                    {
                        item.gameObject.SetActive(false);
                    }
                    else if (item.gameObject.tag == "Drop")
                    {
                        item.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                    }
                }
                StartCoroutine(FadeIn(Panel_ModelSelection, Panel_Shower, ""));
                MenuNavigationAvailable = true;
            }
            else if (CurrentStage == Stage.MakeUpStage)
            {
                if (ShootingStar.isPlaying == false)
                {
                    ShootingStar.Play();
                }
                Panel_MakeUp_Sidebar.SetActive(false);
                Panel_MakeUp_Main.SetActive(false);
                foreach (Transform item in DressUpScene.transform)
                {
                    if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                    {
                        //item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f).OnComplete(() => item.gameObject.SetActive(false));
                        item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                    }
                }
                foreach (Transform item in ShoweringScene.transform)
                {
                    if (item.gameObject.tag != "Drop" && item.gameObject.tag != "WetHair" && item.gameObject.tag != "Model" && item.gameObject.tag != "Foam")
                    {
                        item.gameObject.SetActive(true);
                        item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f).OnComplete(() => MenuNavigationAvailable = true);
                    }
                    else if (item.gameObject.tag == "Model")
                    {
                        if (SelectedModel == SkinTone0 && item.gameObject.name == "Image_SkinTone0")
                        {
                            item.gameObject.SetActive(true);
                            item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                        }
                        else if (SelectedModel == SkinTone1 && item.gameObject.name == "Image_SkinTone1")
                        {
                            item.gameObject.SetActive(true);
                            item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                        }
                        else if (SelectedModel == SkinTone2 && item.gameObject.name == "Image_SkinTone2")
                        {
                            item.gameObject.SetActive(true);
                            item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                        }
                        else if (SelectedModel == SkinTone3 && item.gameObject.name == "Image_SkinTone3")
                        {
                            item.gameObject.SetActive(true);
                            item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                        }
                    }
                }
                ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                ShowerScene.instance.Hand.transform.position = ShowerScene.instance.Shampoo.transform.position;
                CurrentStage = Stage.ShowerStage;
            }
            else if (CurrentStage == Stage.DressUpStage)
            {
                if (ShootingStar.isPlaying == false)
                {
                    ShootingStar.Play();
                }
                if (Panel_Dresses.activeSelf == true)
                {
                    Panel_Dresses.SetActive(false);
                }
                if (Panel_Shoes.activeSelf == true)
                {
                    Panel_Shoes.SetActive(false);
                }
                Panel_DressUpMain.SetActive(false);
                foreach (Transform item in SelectDressScene.transform)
                {
                    item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                    if (item.childCount > 1)
                    {
                        item.GetChild(0).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                        item.GetChild(1).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                    }
                }
                
                DressUpScene.SetActive(true);
                Panel_MakeUp_Main.SetActive(true);
                Panel_MakeUp_Sidebar.SetActive(true);
                foreach (Transform item in DressUpScene.transform)
                {
                    foreach (GameObject item2 in AppliedMakeUp)
                    {
                        if (item2 == item.gameObject)
                        {
                            item.gameObject.SetActive(true);
                            item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                            if (item.childCount > 1)
                            {
                                item.GetChild(0).gameObject.SetActive(true);
                                item.GetChild(1).gameObject.SetActive(true);
                                item.GetChild(0).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                                item.GetChild(1).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                            }
                        }
                    }

                    if (item.gameObject.name == "Img_SkinTone0" && SelectedModel == SkinTone0)
                    {
                        item.gameObject.SetActive(true);
                    }
                    if (item.gameObject.name == "Img_SkinTone1" && SelectedModel == SkinTone1)
                    {
                        item.gameObject.SetActive(true);
                    }
                    if (item.gameObject.name == "Img_SkinTone2" && SelectedModel == SkinTone2)
                    {
                        item.gameObject.SetActive(true);
                    }
                    if (item.gameObject.name == "Img_SkinTone3" && SelectedModel == SkinTone3)
                    {
                        item.gameObject.SetActive(true);
                    }
                    if (item.gameObject.name == "Image_Table")
                    {
                        item.gameObject.SetActive(true);
                    }
                    if (item.gameObject.name == "Image_MakeUp")
                    {
                        item.gameObject.SetActive(true);
                    }
                    if (item.gameObject.name == "DressUpSceneBackground")
                    {
                        item.gameObject.SetActive(true);
                    }
                    if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                    {
                        item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f).OnComplete(() => MenuNavigationAvailable = true);
                    }
                }
                CurrentStage = Stage.MakeUpStage;
            }
            else if (CurrentStage == Stage.ScreenShotStage)
            {
                Mask.SetActive(false);
                ScreenShotFrame.SetActive(false);
                MainCanvas.enabled = true;
                DressCloset.SetActive(true);
                DressUpSelectionImage.SetActive(true);
                Panel_Dresses.SetActive(true);
                NoUnlocks = true;
                PlayVungleAd();
                //Vungle.playAdWithOptions(options);
                CurrentStage = Stage.DressUpStage;
            }
        }
    }
    public void LoadNextScene()
    {
        if (MenuNavigationAvailable == true)
        {
            if (ShootingStar.isPlaying == false)
            {
                ShootingStar.Play();
            }
            if (MenuOpen == true)
            {
                OpenMenu();
            }
            ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            if (CurrentStage == Stage.ModelSelectStage || CurrentStage==Stage.ShowerStage)
            {
                Scrub = true;
                DressUpScene.SetActive(true);
                foreach (Transform item in DressUpScene.transform)
                {
                    item.gameObject.SetActive(false);
                    //if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                    //{
                    //    item.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                    //}
                }
                
                foreach (GameObject item in MakeUpSceneSprites)
                {
                    item.SetActive(true);
                    item.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                }
                StartCoroutine(FadeSprite(ShoweringScene));
                StartCoroutine(UnFadeSprite(DressUpScene));
                StartCoroutine(FadeOut(Panel_Shower));
                StartCoroutine(FadeIn(Panel_MakeUp_Main, Panel_Shower, ""));
                StartCoroutine(FadeIn(HairStyle, Panel_Shower, ""));
                Panel_MakeUp_Sidebar = HairStyle;
                MenuNavigationAvailable = true;
                CurrentStage = Stage.MakeUpStage;
            }
            else if (CurrentStage == Stage.MakeUpStage)
            {

                SelectDressScene.SetActive(true);
                foreach (Transform item in SelectDressScene.transform)
                {
                    item.gameObject.SetActive(false);
                }
                foreach (GameObject item in DressUpSceneSprites)
                {
                    item.SetActive(true);
                    item.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                }
                DressCloset.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                Panel_Dresses.SetActive(true);
                Panel_DressUpMain.SetActive(true);
                DressUpSelectionImage.SetActive(true);
                DressUpSelectionImage.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.2f);
                if (SelectedModel == SkinTone0)
                {
                    BodySkinTone0.SetActive(true);
                    BodySkinTone0.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f).OnComplete(() => MenuNavigationAvailable = true);
                    BodySkinTone1.SetActive(false);
                    BodySkinTone2.SetActive(false);
                    BodySkinTone3.SetActive(false);
                }
                else if (SelectedModel == SkinTone1)
                {
                    BodySkinTone1.SetActive(true);
                    BodySkinTone1.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f).OnComplete(() => MenuNavigationAvailable = true);
                    BodySkinTone0.SetActive(false);
                    BodySkinTone2.SetActive(false);
                    BodySkinTone3.SetActive(false);
                }
                else if (SelectedModel == SkinTone2)
                {
                    BodySkinTone2.SetActive(true);
                    BodySkinTone2.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f).OnComplete(() => MenuNavigationAvailable = true);
                    BodySkinTone1.SetActive(false);
                    BodySkinTone3.SetActive(false);
                    BodySkinTone0.SetActive(false);
                }
                else if (SelectedModel == SkinTone3)
                {
                    BodySkinTone3.SetActive(true);
                    BodySkinTone3.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f).OnComplete(()=>MenuNavigationAvailable=true);
                    BodySkinTone0.SetActive(false);
                    BodySkinTone1.SetActive(false);
                    BodySkinTone2.SetActive(false);
                }
                Dresses[0].GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                AppliedMakeUp = new List<GameObject>();
                foreach (Transform item in DressUpScene.transform)
                {
                    if ((item.gameObject.activeSelf == true) && (item.gameObject.tag != "BG") && (item.gameObject.tag != "Model") && (item.gameObject.name != "Image_Table"))
                    {
                        AppliedMakeUp.Add(item.gameObject);
                    }
                }
                //Removes all makeup before applying new one
                foreach (Transform itemx in SelectDressScene.transform)
                {
                    if (itemx.gameObject.name[0].Equals('A'))
                    {
                        if (itemx.gameObject.activeSelf == (true))
                        {
                            itemx.gameObject.SetActive(false);
                        }    
                    }
                }
                foreach (GameObject item in AppliedMakeUp)
                {
                    foreach (Transform item2 in SelectDressScene.transform)
                    {
                        if (item2.gameObject.name == ("A_" + item.name))
                        {
                            item2.gameObject.SetActive(true);
                            if (item2.childCount <= 1)
                            {
                                if (item2.gameObject.tag == "A_Gloss")
                                {
                                    item2.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0.275f), 0.25f);
                                }
                                else
                                {
                                    item2.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.25f);
                                }
                            }
                            else if (item2.childCount > 1)
                            {
                                item2.GetChild(0).gameObject.SetActive(true);
                                item2.GetChild(1).gameObject.SetActive(true);
                                item2.GetChild(0).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.2f);
                                item2.GetChild(1).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.2f);
                            }
                        }
                    }
                }
                StartCoroutine(FadeSprite(DressUpScene));
                foreach (Transform item in SelectDressScene.transform)
                {
                    if (item.gameObject.name == "DressUpSceneBackground")
                    {
                        item.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                    }
                }
                CurrentStage = Stage.DressUpStage;
                Panel_MakeUp_Main.SetActive(false);
                Panel_MakeUp_Sidebar.SetActive(false);
            }
            else if (CurrentStage == Stage.DressUpStage)
            {
                ScreenShotButtonCall();
            }
        }
    }
    public void Deactivate(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }
    public void ScreenShotButtonCall()
    {
        Destroy(Tex);
        if (CurrentStage == Stage.DressUpStage)
        {
            if (System.IO.File.Exists(Application.persistentDataPath + "/ScreenShot.png"))
            {
                Debug.Log("DELETING PREVIOUS FILE....");
                System.IO.File.Delete(Application.persistentDataPath + "/ScreenShot.png");
            }
            if (System.IO.File.Exists(Application.persistentDataPath + "/ScreenShot.png"))
            {
                Debug.Log("COULD NOT DELETE PREVIOUS FILE...");
            }
            Debug.Log(CurrentStage);
            CurrentStage = Stage.ScreenShotStage;
            StartCoroutine(Shot());
        }
    }
    public IEnumerator Shot()
    {
        MainCanvas.enabled = false;
        if (ScreenShotFrame.activeSelf == true)
        {
            ScreenShotFrame.SetActive(false);
        }
        DressCloset.SetActive(false);
        DressUpSelectionImage.SetActive(false);
        if (Panel_Dresses.activeSelf == true)
        {
            Activated = 1;
            Panel_Dresses.SetActive(false);
        }
        
        else if (Panel_Shoes.activeSelf == true)
        {
            Activated = 2;
            Panel_Shoes.SetActive(false);
        }
        Panel_DressUpMain.SetActive(false);
        Screen_Shot_File_Name = "ScreenShot.png";
        Application.CaptureScreenshot(Screen_Shot_File_Name);
        Debug.Log("Ali Ameen: ScreenShotCaptured.. waiting for save...");
        yield return new WaitUntil(()=> (System.IO.File.Exists(Application.persistentDataPath+ "/ScreenShot.png")));
        ShowScreenShot();
        //File.Copy(Application.persistentDataPath + "/ScreenShot.png", "/mnt/sdcard/DCIM/ScreenShot.png");
        //SaveScreenShotToGallery();
        //RefreshGallery.refreshGallery(Application.persistentDataPath, "/ScreenShot.png");
        //Debug.Log("Ali Ameen: Screenshot exists...");
        ScreenShotTaken = true;
    }
    public void OpenMenu()
    {
        MenuOpen = !MenuOpen;
        if (MenuOpen == true)
        {
            MenuButton.GetComponent<Image>().sprite = CrossIcon;
            VolumeButton.SetActive(true);
            CartButton.SetActive(true);
            HomeButton.SetActive(true);
            ScreenShotButton.SetActive(true);
            RateButton.SetActive(true);

            VolumeButton.transform.position = MenuButton.transform.position;
			VolumeButton.transform.DOMove(new Vector3(VolumeButton.transform.position.x, VolumeButton.transform.position.y-1,VolumeButton.transform.position.z), 0.1f, false);

            if (CurrentStage != Stage.DressUpStage)
            {
                ScreenShotButton.GetComponent<Button>().interactable = false;
            }
            else if (CurrentStage == Stage.DressUpStage)
            {
                ScreenShotButton.GetComponent<Button>().interactable = true;
            }

            ScreenShotButton.transform.position = MenuButton.transform.position;
			ScreenShotButton.transform.DOMove(new Vector3(ScreenShotButton.transform.position.x, ScreenShotButton.transform.position.y-2,ScreenShotButton.transform.position.z), 0.15f, false);

            RateButton.transform.position = MenuButton.transform.position;
			RateButton.transform.DOMove(new Vector3(RateButton.transform.position.x, RateButton.transform.position.y-3,RateButton.transform.position.z) , 0.2f, false);
            
            CartButton.transform.position = MenuButton.transform.position;
			CartButton.transform.DOMove(new Vector3(CartButton.transform.position.x+1, CartButton.transform.position.y,CartButton.transform.position.z) , 0.1f, false);
            
            HomeButton.transform.position = MenuButton.transform.position;
			HomeButton.transform.DOMove(new Vector3(HomeButton.transform.position.x+2, HomeButton.transform.position.y,HomeButton.transform.position.z), 0.15f, false);
        }
        else if (MenuOpen == false)
        {
            MenuButton.GetComponent<Image>().sprite = MenuIcon;
            VolumeButton.transform.DOMove(MenuButton.transform.position, 0.1f, false).OnComplete(() => VolumeButton.SetActive(false));
            ScreenShotButton.transform.DOMove(MenuButton.transform.position, 0.15f, false).OnComplete(() => ScreenShotButton.SetActive(false));
            RateButton.transform.DOMove(MenuButton.transform.position, 0.2f, false).OnComplete(() => RateButton.SetActive(false));
            CartButton.transform.DOMove(MenuButton.transform.position, 0.1f, false).OnComplete(() => CartButton.SetActive(false));
            HomeButton.transform.DOMove(MenuButton.transform.position, 0.15f, false).OnComplete(() => HomeButton.SetActive(false));
        } 
    }
    public void Share()
    {
        //instantiate the class Intent
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");

        //instantiate the object Intent
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        //call setAction setting ACTION_SEND as parameter
        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));

        //instantiate the class Uri
        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");

        //instantiate the object Uri with the parse of the url's file
        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", Application.persistentDataPath + "/ScreenShot.png");

        //call putExtra with the uri object of the file
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);

        //set the type of file
        intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");

        //instantiate the class UnityPlayer
        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        //instantiate the object currentActivity
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        //call the activity with our Intent
        currentActivity.Call("startActivity", intentObject);
    }
    public void ShowScreenShot()
    {

        NoUnlocks = true;
        if (System.IO.File.Exists(Application.persistentDataPath + "/ScreenShot.png")) 
        {
            Debug.Log("ALI AMEEN: Screenshot was successfully saved in PersistentDataPath....");
            
            Tex = new Texture2D(Mathf.RoundToInt(ScreenShotFrame.GetComponent<RectTransform>().rect.width), Mathf.RoundToInt(ScreenShotFrame.GetComponent<RectTransform>().rect.height));
            try
            {
                Tex.LoadImage(System.IO.File.ReadAllBytes(Application.persistentDataPath + "/ScreenShot.png"));
            }
            catch
            {
                Debug.Log("ALI AMEEN: FILE LOADING FAILED.....");
                ShowScreenShot();
            }
            
            Sprite Texture = Sprite.Create(Tex, new Rect(0, 0, Screen.width, Screen.height), new Vector2(0.5f, 0.5f));
            ScreenShotFrame.GetComponent<Image>().sprite = Texture;
        }
        if (Activated == 1)
        {
            Panel_Dresses.SetActive(true);
        }

        else if (Activated == 2)
        {
            Panel_Shoes.SetActive(true);
        }
        Mask.SetActive(true);
        ScreenShotFrame.SetActive(true);
        DressUpSelectionImage.SetActive(true);
        DressCloset.SetActive(true);
        Panel_DressUpMain.SetActive(true);
        MainCanvas.enabled = true;
        OpenMenu();
        Debug.Log("Ali Ameen: I AM STANDING HERE....");
        StopCoroutine(Shot());       
    }
    public void SaveScreenShotToGallery()
    {
        Texture2D Tex = new Texture2D(Screen.width, Screen.height);
        Tex.LoadImage(System.IO.File.ReadAllBytes(Application.persistentDataPath + "/ScreenShot.png"));
        if (Application.platform == RuntimePlatform.Android)
        {

			#if UNITY_ANDROID
            AGGallery.SaveImageToGallery(Tex, "ScreenShot", "Magic Queen Dress Up Game", ImageFormat.PNG);
       
			#endif
			}
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            IOSCamera.Instance.SaveTextureToCameraRoll(Tex);
        }
    }
    public IEnumerator TakeMenuAway()
    {
        float Duration = 2.5f;
        float StartTime = Time.time;

        while (Button_Play.transform.position.x > -1000)
        {
            while (Time.time - StartTime < Duration)
            {
                Button_Play.transform.localPosition = Vector3.Lerp(Button_Play.transform.localPosition, new Vector3(Button_Play.transform.localPosition.x - 1000, Button_Play.transform.localPosition.y, Button_Play.transform.localPosition.z), ((Time.time - StartTime) / (Duration * 3f)));
                Button_Store.transform.localPosition = Vector3.Lerp(Button_Store.transform.localPosition, new Vector3(Button_Store.transform.localPosition.x - 1000, Button_Store.transform.localPosition.y, Button_Store.transform.localPosition.z), ((Time.time - StartTime) / (Duration * 2)));
                Button_Settings.transform.localPosition = Vector3.Lerp(Button_Settings.transform.localPosition, new Vector3(Button_Settings.transform.localPosition.x - 1000, Button_Settings.transform.localPosition.y, Button_Settings.transform.localPosition.z), ((Time.time - StartTime) / (Duration)));
                Button_Quit.transform.localPosition = Vector3.Lerp(Button_Quit.transform.localPosition, new Vector3(Button_Quit.transform.localPosition.x - 1000, Button_Quit.transform.localPosition.y, Button_Quit.transform.localPosition.z), ((Time.time - StartTime) / (Duration * 0.25f)));
                //Button_GetFreeCoins.transform.localPosition = Vector3.Lerp(Button_GetFreeCoins.transform.localPosition, new Vector3(Button_GetFreeCoins.transform.localPosition.x + 1000, Button_GetFreeCoins.transform.localPosition.y, Button_GetFreeCoins.transform.localPosition.z), ((Time.time - StartTime) / Duration));
                yield return null;
            }
            yield return null;
        }
    }
    public IEnumerator Translate(GameObject gameObject, Vector3 InitPos, Vector3 FinalPos, float Duration)
    {
        float StartTime = Time.time;

        while (Time.time - StartTime < Duration)
        {
            gameObject.transform.localPosition = Vector3.Lerp(gameObject.transform.localPosition, FinalPos, ((Time.time - StartTime) / (Duration * 3f)));
            yield return null;
        }
    }
    public IEnumerator FadeOut(GameObject gameObject)
    {
        while (gameObject.GetComponent<Image>().color.a >= 0)
        {
            gameObject.GetComponent<Image>().color -= new Color(0, 0, 0, Time.deltaTime * 4);
            foreach (Transform item in gameObject.transform)
            {
                if (item.gameObject.GetComponent<Image>() != null)
                {
                    item.gameObject.GetComponent<Image>().color -= new Color(0, 0, 0, Time.deltaTime * 6);
                }
            }
            yield return null;
        }
        gameObject.SetActive(false);
        yield return null;
    }
    public IEnumerator FadeIn(GameObject NewMenu, GameObject OldMenu, string Name)
    {
        NewMenu.SetActive(true);
        Color RefColor = new Color(1, 1, 1, 1);
        if (NewMenu != Panel_ModelSelection)
        {
            foreach (Transform item in NewMenu.transform)
            {
                if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                {
                    item.gameObject.GetComponent<SpriteRenderer>().color += new Color(0, 0, 0, (RefColor.a - item.gameObject.GetComponent<SpriteRenderer>().color.a)/1.0f);
                }
            } 
        }
        switch (Name)
        {
            case "ModelSelection_BlueDress":
                {
                    SelectedModel = SkinTone0;
                    SelectedModel.SetActive(true);
                    SelectedModel.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                    break;
                }
            case "ModelSelection_GreenDress":
                {
                    SelectedModel = SkinTone1;
                    SelectedModel.SetActive(true);
                    SelectedModel.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                    break;
                }
            case "ModelSelection_WhiteDress":
                {
                    SelectedModel = SkinTone2;
                    SelectedModel.SetActive(true);
                    SelectedModel.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                    break;
                }
            case "ModelSelection_BlackDress":
                {
                    SelectedModel = SkinTone3;
                    SelectedModel.SetActive(true);
                    SelectedModel.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                    break;
                }                
        }
        if (NewMenu.GetComponent<Image>() != null)
        {
            while (NewMenu.GetComponent<Image>().color.a < 1)
            {
                if (NewMenu == Panel_ModelSelection)
                {
                    NewMenu.GetComponent<Image>().color += new Color(0, 0, 0, Time.deltaTime * 4);
                }
                foreach (Transform item in NewMenu.transform)
                {
                    if (item.gameObject.GetComponent<Image>() != null)
                    {
                        item.gameObject.GetComponent<Image>().color += new Color(0, 0, 0, Time.deltaTime * 6);
                    }
                }
                yield return null;
            }
        }
        OldMenu.SetActive(false);
        yield return null;
    }
    public IEnumerator FadeSprite(GameObject Sprites)
    {
        SpriteRenderer[] Rends = Sprites.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer item in Rends)
        {
            if (item.gameObject.tag != "Model")
            {
                item.DOColor(new Color(1, 1, 1, 0), 0.25f);
            }
            else if (item.gameObject.tag == "Model" || item.gameObject.tag == "Hair" || item.gameObject.tag == "BG")
            {
                item.color = new Color(1, 1, 1, 0);
            }
            else if (item.gameObject.tag == "Foam")
            {
                item.gameObject.SetActive(false);
            }
        }
        yield return new WaitForSeconds(0.3f);
        foreach (SpriteRenderer item in Rends)
        {
            if (item.gameObject.tag != "Hand")
            {
                item.gameObject.SetActive(false);
            }
        }
        yield return null;
    }
    public IEnumerator UnFadeSprite(GameObject Sprites)
    {
        Sprites.SetActive(true);
        SpriteRenderer[] Rends = Sprites.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer item in Rends)
        {
            if (item.gameObject.tag != "Drop" && item.gameObject.tag != "WetHair")
            {
                if (item.gameObject.tag != "Model" && item.gameObject.tag != "Hair")
                {
                    item.DOColor(new Color(1, 1, 1, 1), 0.1f);
                }
                else if (item.gameObject.tag == "Model" || item.gameObject.tag == "Hair" || item.gameObject.tag == "BG")
                {
                    if (item.gameObject.tag == "Hair")
                    {
                        item.color = new Color(1, 1, 1, 1);
                    }
                    if (SelectedModel == SkinTone0)
                    {
                        MKTone0.SetActive(true);
                        MKTone0.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                        //Debug.Log("SkinTone0");
                        //Debug.Log(item.gameObject.name);
                        //item.color = new Color(1, 1, 1, 1);
                    }
                    else if (SelectedModel == SkinTone1)
                    {
                        MKTone1.SetActive(true);
                        MKTone1.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);

                        //Debug.Log("SkinTone1");
                        //Debug.Log(item.gameObject.name);
                        //item.color = new Color(1, 1, 1, 1);
                    }
                    else if (SelectedModel == SkinTone2)
                    {
                        MKTone2.SetActive(true);
                        MKTone2.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);

                        //Debug.Log("SkinTone2");
                        //Debug.Log(item.gameObject.name);
                        //item.color = new Color(1, 1, 1, 1);
                    }
                    else if (SelectedModel == SkinTone3)
                    {
                        MKTone3.SetActive(true);
                        MKTone3.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);

                        //Debug.Log("SkinTone3");
                        //Debug.Log(item.gameObject.name);
                        //item.color = new Color(1, 1, 1, 1);
                    }
                }
            }
        }
        yield return null;
    }
    public IEnumerator ToGallery()
    {
        yield return new WaitForSeconds(1.5f);
    }
    public void OpenPurchaseDialog(GameObject Clicked)
    {
        ToBeUnlocked = Clicked;
        NoUnlocks = false;
        if (UI_Control.instance.MenuOpen == true)
        {
            UI_Control.instance.OpenMenu();
        }
        Panel_PurchaseThisItem.SetActive(true);
        PriceText.GetComponent<Text>().text = "Price: " + "$0.99";
        ItemImage.SetActive(true);
        ItemImage.GetComponent<Image>().sprite = Clicked.GetComponent<Image>().sprite;
        ItemImage.GetComponent<Image>().preserveAspect = true;
        BuyButton.GetComponent<Button>().onClick.RemoveAllListeners();
        BuyButton.GetComponent<Button>().onClick.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.EditorAndRuntime);
        //ItemImage.GetComponent<RectTransform>().rect.height = 
    }
    public void OpenUnlockAllDialog()
    {
        allProducts = true;
        OpenAskYourParents();
    }
    public void ClosePurchaseDialog()
    {
        if (Panel_PurchaseThisItem != null && Panel_PurchaseThisItem == true)
        {
            Panel_PurchaseThisItem.SetActive(false);
        }
    }
    public void ClosePanelUnlocked()
    {
        Panel_Unlocked.SetActive(false);
    }
    public void GoToHomePage()
    {
        if (ScreenShotFrame.activeSelf == true)
        {
            ScreenShotFrame.SetActive(false);
        }
        if (UI_Control.instance.BodySkinTone0.activeSelf == true)
        {
            UI_Control.instance.BodySkinTone0.GetComponent<SpriteRenderer>().sprite = UI_Control.instance.Reg0;
        }
        if (UI_Control.instance.BodySkinTone1.activeSelf == true)
        {
            UI_Control.instance.BodySkinTone1.GetComponent<SpriteRenderer>().sprite = UI_Control.instance.Reg1;
        }
        if (UI_Control.instance.BodySkinTone2.activeSelf == true)
        {
            UI_Control.instance.BodySkinTone2.GetComponent<SpriteRenderer>().sprite = UI_Control.instance.Reg2;
        }
        if (UI_Control.instance.BodySkinTone3.activeSelf == true)
        {
            UI_Control.instance.BodySkinTone3.GetComponent<SpriteRenderer>().sprite = UI_Control.instance.Reg3;
        }
        if (MenuNavigationAvailable == true)
        {
            ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            Debug.Log(CurrentStage.ToString());
            if (CurrentStage == Stage.ModelSelectStage || CurrentStage == Stage.ShowerStage)
            {
                if (ShootingStar.isPlaying == false)
                {
                    ShootingStar.Play();
                }
                Scrub = false;
                if (MenuOpen == true)
                {
                    OpenMenu();
                }
                foreach (Transform item in ShoweringScene.transform)
                {
                    if (item.gameObject.tag == "Foam")
                    {
                        item.gameObject.SetActive(false);
                    }
                    else if (item.gameObject.tag == "Drop")
                    {
                        item.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                    }
                }
                MenuNavigationAvailable = true;
            }
            else if (CurrentStage == Stage.ModelSelectStage)
            {
                if (ShootingStar.isPlaying == false)
                {
                    ShootingStar.Play();
                }
                foreach (Transform item in Panel_ModelSelection.transform)
                {
                    item.gameObject.GetComponent<Image>().DOColor(new Color(1, 1, 1, 0), 0.5f);
                }
                Panel_ModelSelection.SetActive(false);
            }
            else if (CurrentStage == Stage.MakeUpStage)
            {
                if (ShootingStar.isPlaying == false)
                {
                    ShootingStar.Play();
                }
                Panel_MakeUp_Sidebar.SetActive(false);
                Panel_MakeUp_Main.SetActive(false);
                foreach (Transform item in DressUpScene.transform)
                {
                    if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                    {
                        //item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f).OnComplete(() => item.gameObject.SetActive(false));
                        item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                    }
                }
                ShowerScene.instance.Hand.transform.position = ShowerScene.instance.Shampoo.transform.position;
            }
            else if (CurrentStage == Stage.DressUpStage)
            {
                if (ShootingStar.isPlaying == false)
                {
                    ShootingStar.Play();
                }
                if (Panel_Dresses.activeSelf == true)
                {

                    Panel_Dresses.SetActive(false);
                }
                if (Panel_Shoes.activeSelf == true)
                {
                    Panel_Shoes.SetActive(false);
                }
                Panel_DressUpMain.SetActive(false);
                foreach (Transform item in SelectDressScene.transform)
                {
                    item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                    if (item.childCount > 1)
                    {
                        item.GetChild(0).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                        item.GetChild(1).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                    }
                }
            }
            else if (CurrentStage == Stage.ScreenShotStage)
            {
                Mask.SetActive(false);
                ScreenShotFrame.SetActive(false);
                CurrentStage = Stage.DressUpStage;
                
                if (ShootingStar.isPlaying == false)
                {
                    ShootingStar.Play();
                }
                if (Panel_Dresses.activeSelf == true)
                {

                    Panel_Dresses.SetActive(false);
                }
                if (Panel_Shoes.activeSelf == true)
                {
                    Panel_Shoes.SetActive(false);
                }
                Panel_DressUpMain.SetActive(false);
                foreach (Transform item in SelectDressScene.transform)
                {
                    item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                    if (item.childCount > 1)
                    {
                        item.GetChild(0).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                        item.GetChild(1).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.25f);
                    }
                }
            }
            Panel_MainMenu.SetActive(true);
            Panel_MainMenu.GetComponent<Image>().color = new Color(1, 1, 1, 1);

            foreach (Transform item in Panel_MainMenu.transform)
            {
                item.gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            }

            Button_Play.transform.position = OrigPosStart;
            Button_Store.transform.position = OrigPosStore;
            Button_Settings.transform.position = OrigPosSettings;
            Button_Quit.transform.position = OrigPosQuit;
            //Button_GetFreeCoins.transform.position = OrigPosCoins;
            NameField.transform.position = OrigPosNameField;
            Button_Enter.transform.position = OrigPosEnter;
            CurrentStage = Stage.StartStage;
        }
    }
    public void QuitApplication()
    {
        Application.Quit();
    }
    void OnApplicationQuit()
    {
        PlayerPrefs.Save();
    }
    public void ToggleSounds(GameObject caller)
    {
        if (ShootingStar.GetComponent<AudioSource>().enabled == true)
        {
            ShootingStar.GetComponent<AudioSource>().enabled = false;
        }
        else if (ShootingStar.GetComponent<AudioSource>().enabled == false)
        {
            ShootingStar.GetComponent<AudioSource>().enabled = true;
        }
        if (BGMusic.GetComponent<AudioSource>().enabled == true)
        {
            BGMusic.GetComponent<AudioSource>().enabled = false;
        }
        else if (BGMusic.GetComponent<AudioSource>().enabled == false)
        {
            BGMusic.GetComponent<AudioSource>().enabled = true;
        }
        if (MakeUpController.instance.MakeUpSound.GetComponent<AudioSource>().enabled == true)
        {
            MakeUpController.instance.MakeUpSound.GetComponent<AudioSource>().enabled = false;
        }
        else if (MakeUpController.instance.MakeUpSound.GetComponent<AudioSource>().enabled == false)
        {
            MakeUpController.instance.MakeUpSound.GetComponent<AudioSource>().enabled = true;
        }
        if (MakeUpController.instance.MakeUpApplied.GetComponent<AudioSource>().enabled == true)
        {
            MakeUpController.instance.MakeUpApplied.GetComponent<AudioSource>().enabled = false;
        }
        else if (MakeUpController.instance.MakeUpApplied.GetComponent<AudioSource>().enabled == false)
        {
            MakeUpController.instance.MakeUpApplied.GetComponent<AudioSource>().enabled = true;
        }

        if (VolumeButton.GetComponent<Image>().sprite == VolumeOff)
        {
            VolumeButton.GetComponent<Image>().sprite = VolumeOn;
        }
        else if (VolumeButton.GetComponent<Image>().sprite == VolumeOn)
        {
            VolumeButton.GetComponent<Image>().sprite = VolumeOff;
        }
        
    }
    public void UnlockThisItemPermanently(GameObject Obj)
    {
        GameObject[] array = new GameObject[] { };
        switch (Obj.tag)
        {
            case "Dress":
                for (int i = 0; i < DressUpScenePanel.Length; i++)
                {
                    if (DressUpScenePanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(DressLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        DressLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("DressLocker", DressLocker);
                    }
                }
                break;
            case "Shoes":
                for (int i = 0; i < ShoesPanel.Length; i++)
                {
                    if (ShoesPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(ShoesLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        ShoesLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("ShoesLocker", ShoesLocker);
                    }
                }
                break;
            case "HairStyle":
                for (int i = 0; i < HairPanel.Length; i++)
                {
                    if (HairPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(HairLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        HairLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("HairLocker", HairLocker);
                    }
                }
                break;
            case "LipStick":
                Debug.Log(Obj.name);
                for (int i = 0; i < LipStickPanel.Length; i++)
                {
                    if (LipStickPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(LipStickLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        LipStickLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("LipStickLocker", LipStickLocker);
                    }
                }
                break;
            case "LipGloss":
                for (int i = 0; i < LipGlossPanel.Length; i++)
                {
                    if (LipGlossPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(LipGlossLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        LipGlossLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("LipGlossLocker", LipGlossLocker);
                    }
                }
                break;
            case "EyeLens":
                for (int i = 0; i < EyeLensPanel.Length; i++)
                {
                    if (EyeLensPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(EyeLensLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        EyeLensLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("EyeLensLocker", EyeLensLocker);
                    }
                }
                break;
            case "HairAccessories":
                for (int i = 0; i < HairBandPanel.Length; i++)
                {
                    if (HairBandPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(HairBandLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        HairBandLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("HairBandLocker", HairBandLocker);
                    }
                }
                break;
            case "Glasses":
                for (int i = 0; i < GlassesPanel.Length; i++)
                {
                    if (GlassesPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(GlassesLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        GlassesLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("GlassesLocker", GlassesLocker);
                    }
                }
                break;
            case "EyeBrows":
                for (int i = 0; i < EyeBrowsPanel.Length; i++)
                {
                    if (EyeBrowsPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(EyeBrowsLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        EyeBrowsLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("EyeBrowsLocker", EyeBrowsLocker);
                    }
                }
                break;
            case "EarRing":
                for (int i = 0; i < EarringsPanel.Length; i++)
                {
                    if (EarringsPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(EarringsLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        EarringsLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("EarringsLocker", EarringsLocker);
                    }
                }
                break;
            case "EyeShadow":
                for (int i = 0; i < EyeShadowPanel.Length; i++)
                {
                    if (EyeShadowPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(EyeShadowLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        EyeShadowLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("EyeShadowLocker", EyeShadowLocker);
                    }
                }
                break;
            case "EyeLiner":
                for (int i = 0; i < EyeLinerPanel.Length; i++)
                {
                    if (EyeLinerPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(EyeLinerLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        EyeLinerLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("EyeLinerLocker", EyeLinerLocker);
                    }
                }
                break;
            case "EyeLashes":
                for (int i = 0; i < EyeLashesPanel.Length; i++)
                {
                    if (EyeLashesPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(EyeLashesLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        EyeLashesLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("EyeLashesLocker", EyeLashesLocker);
                    }
                }
                break;
            case "Blushes":
                for (int i = 0; i < BlushesPanel.Length; i++)
                {
                    if (BlushesPanel[i].name == Obj.name)
                    {
                        StringBuilder BuilderString = new StringBuilder(BlushesLocker);
                        BuilderString[i] = "0".ToCharArray()[0];
                        BlushesLocker = BuilderString.ToString();
                        PlayerPrefs.SetString("BlushesLocker", BlushesLocker);
                    }
                }
                break;
        }
    }

    public void OpenAskYourParents(GameObject Clicked = null)
    {
        answerField.text = "";
        clickedProduct = Clicked;
        askYourParentsPopUp.SetActive(true);
        int firstNum = Random.Range(1, 9);
        int secondNum = Random.Range(1, 9);
        int operation = Random.Range(1, 3);
        string operationCharacter = "+";
        switch (operation)
        {
            case 1:
                //Addition
                correctAnswer = firstNum + secondNum;
                operationCharacter = "+";
                break;
            case 2:
                //Multiplication
                correctAnswer = firstNum * secondNum;
                operationCharacter = "x";
                break;
        }

        askYourParentsText[0].text = firstNum.ToString();
        askYourParentsText[1].text = operationCharacter.ToString();
        askYourParentsText[2].text = secondNum.ToString();
    }
    public void CompareAnswer()
    {
        int answer = int.Parse(answerField.text.ToString());
        if (answer == correctAnswer)
        {
            askYourParentsPopUp.SetActive(false);
            Debug.Log("Right answer");
            if (allProducts)
            {
                if (UI_Control.instance.MenuOpen == true)
                {
                    UI_Control.instance.OpenMenu();
                }
                Panel_UnlockAll.SetActive(true);
                UnlockAllButton.GetComponent<Button>().onClick.SetPersistentListenerState(0, UnityEngine.Events.UnityEventCallState.Off);
                UnlockAllButton.GetComponent<Button>().onClick.AddListener(() => Purchaser.instance.UnlockAll());
            }
            else
            {
                OpenPurchaseDialog(clickedProduct);
            }
        }
        else
        {
            askYourParentsPopUp.SetActive(false);
            Debug.Log("Wrong answer");
        }
        if (MenuOpen)
        {
            OpenMenu();
        }
        allProducts = false;
    }


}